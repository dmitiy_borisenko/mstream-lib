//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "TcpServer.h"

#include <QTcpServer>
#include <QtNetwork>
#include <unistd.h>
#include <stdexcept>

//#include "mpd-common/PrintInfo.h"

//const int TCP_SERVER_BUFFER_SIZE = 10000;
// EventSize(s)=40+4*4*s; EventSize(200)=3240; 10Mb for 3236 Events
namespace {
const int TCP_SERVER_BUFFER_SIZE = 10*1024*1024;
const int SOCKET_PENDING_DATA_LIMIT = TCP_SERVER_BUFFER_SIZE;
const int SEND_TIMEOUT_MS = 10;
const int UPDATE_TIMEOUT_MS = 1000;
}

TcpServer::TcpServer(int port, QObject *parent, QString _logPrefix) :
    QObject(parent),
    logPrefix(_logPrefix),
    tcpPort(port),
    qTcpServer(new QTcpServer(this)),
    tcpSocket(nullptr),
    sendTimer(new QTimer(this)),
    updateTimer(new QTimer(this)),
    slow_connection_timer(new QElapsedTimer()),
    slow_connection_duration(0)
{
    reset();

    logPrefix = QString("[TcpServer] [%1]: ").arg(logPrefix);

    while(!qTcpServer->listen(QHostAddress::Any, tcpPort)) {
        QAbstractSocket::SocketError errNum = qTcpServer->serverError();
        if(errNum==QAbstractSocket::AddressInUseError){
            qWarning().noquote() << logPrefix + tr("Unable to start server on port %1: %2, exiting...").arg(tcpPort).arg(qTcpServer->errorString());
            // sleep(5);
            exit(-1);
            continue;
        }
        QString s = tr("Unable to start server on port %1: %2").arg(tcpPort).arg(qTcpServer->errorString());
        qWarning().noquote() << logPrefix + s;
        throw std::runtime_error(s.toStdString());
    }
    tcpPort = qTcpServer->serverPort();
    status.tcpPort = tcpPort;
    connect(sendTimer, &QTimer::timeout, this, &TcpServer::on_sendTimer_timeout);
    sendTimer->setSingleShot(true);
    sendTimer->setInterval(SEND_TIMEOUT_MS);

    connect(updateTimer, &QTimer::timeout, this, &TcpServer::updateStatus);
    updateTimer->setInterval(UPDATE_TIMEOUT_MS);
    updateTimer->start();

    connect(qTcpServer, &QTcpServer::newConnection,
            this, &TcpServer::updateConnections);
    qInfo().noquote() << logPrefix + QString("TcpServer started on %1:%2").arg(qTcpServer->serverAddress().toString()).arg(tcpPort);
}

TcpServer::~TcpServer()
{
    qint64 bytes = getDataSize();
    if (tcpSocket)
        bytes += tcpSocket->bytesToWrite();
    if (bytes)
        qWarning().noquote() << logPrefix + QString("TcpServer not empty and destroyed, %1 bytes lost").arg(bytes);
    delete slow_connection_timer;
}

void TcpServer::handleDisconnect()
{
    if (!m_connectionName.isEmpty())
        qInfo().noquote() << logPrefix + QString("Closed connection from %1").arg(m_connectionName);
    deleteSocket();
    updateStatus();
}

void TcpServer::checkPendingData()
{
    if(!requestFinish)
        return;
    if((getDataSize()==0) && tcpSocket && (tcpSocket->bytesToWrite()==0))
        emit workCompleted();
}

void TcpServer::updateStatus()
{
    emit statusUpdated(status);
}

void TcpServer::reset()
{
    writePos = 0;
    readPos = TCP_SERVER_BUFFER_SIZE-1;
//    freeSpace.release(TCP_SERVER_BUFFER_SIZE-freeSpace.available());
    slowConnection = false;
    numberBytesSent = 0;
    numberBytesDropped = 0;
    dropOnFree = true;
    requestFinish = false;

    data.clear();
    data.resize(TCP_SERVER_BUFFER_SIZE);

    QTimer::singleShot(0, this, &TcpServer::updateStatus);
}

void TcpServer::deleteSocket()
{
    tcpSocket->deleteLater();
    tcpSocket = nullptr;
    status.conHost.clear();
    status.conPort = 0;
    status.isFree = isFree();
    numberBytesSent = 0;
    numberBytesDropped = 0;
    m_connectionName = QString();
}

bool TcpServer::isEnoughSpace(int size) const
{
    int freeWriteSize = getFreeSize();
    if(freeWriteSize < size)
        return false;

    double freeSizeRate = 1.*freeWriteSize/TCP_SERVER_BUFFER_SIZE;
    if(freeSizeRate<0.5){
        // Enable porosity
        double chance = 1.*qrand()/RAND_MAX;
        if(chance>freeSizeRate)
            //Skipp this event
            return false;
    }
    return true;
}

double TcpServer::getOccupancy() const
{
    return 1.*getDataSize()/TCP_SERVER_BUFFER_SIZE;
}

QString TcpServer::connectionName() const
{
    Q_ASSERT(tcpSocket);
    return connectionName(*tcpSocket);
}

QString TcpServer::connectionName(const QTcpSocket &socket) const
{
    return QString("%1:%2").arg(socket.peerAddress().toString()).arg(socket.peerPort());
}

void TcpServer::on_sendTimer_timeout()
{
    sendData();
    if (tcpSocket)
        sendTimer->start(SEND_TIMEOUT_MS);
}

void TcpServer::sendData()
{
    //wait data available
    int sentSize = 0;
    int totalSize = getDataSize();
    if(totalSize==0){
        writerWC.wakeAll();
        return;
    }
    //Check connetion
    if (!tcpSocket) {
        return;
    }

    if(tcpSocket->bytesToWrite()>SOCKET_PENDING_DATA_LIMIT){
        if(!slowConnection){
            slowConnection = true;
            slow_connection_timer->start();
        }
        return;
    }
    if(slowConnection){
        slowConnection = false;
        if(slow_connection_timer->isValid())
            slow_connection_duration += slow_connection_timer->restart();
    }
    if(tcpSocket->isValid() && tcpSocket->state() == QTcpSocket::ConnectedState){
        int tailSize = TCP_SERVER_BUFFER_SIZE-readPos-1;
        if(tailSize && tailSize < totalSize) {
            if(tailSize<=0) {
                qCritical() << QString("[%1] write<0 hook-1: tailSize=%2; readPos=%3;"
                                       " writePos=%4; TCP_SERVER_BUFFER_SIZE=%5")
                               .arg(logPrefix).arg(tailSize)
                               .arg(readPos).arg(writePos)
                               .arg(TCP_SERVER_BUFFER_SIZE);
            }
            sentSize = tcpSocket->write(data.constData()+readPos+1, tailSize);
            Q_ASSERT(sentSize != -1);
            if(sentSize > 0){
                readPos += tailSize; // TCP_SERVER_BUFFER_SIZE-1
//                freeSpace.release(tailSize);
           } else {
                qWarning().noquote() << logPrefix + "Can't sent data to socket:"  << tcpSocket->errorString();
                return;
            }
        }
        if(readPos == TCP_SERVER_BUFFER_SIZE-1)
            readPos = -1;

        if(totalSize-sentSize<=0) {
            qCritical() << QString("[%1] write<0 hook-2: tailSize=%2; readPos=%3;"
                                   " writePos=%4; TCP_SERVER_BUFFER_SIZE=%5")
                           .arg(logPrefix).arg(tailSize)
                           .arg(readPos).arg(writePos)
                           .arg(TCP_SERVER_BUFFER_SIZE);
        }
        tcpSocket->write(data.constData()+readPos+1, totalSize-sentSize);
        readPos+=totalSize-sentSize;
//        freeSpace.release(totalSize-sentSize);
        sentSize+=totalSize-sentSize;
        writerWC.wakeAll();
        numberBytesSent += sentSize;
        emit bytesSent(numberBytesSent);
    } else {
        deleteSocket();
        updateStatus();
        sendTimer->stop();
    }

    if(requestFinish)
        checkPendingData();
}


void TcpServer::updateConnections()
{
    while(QTcpSocket *pendingConnection = qTcpServer->nextPendingConnection()){
        if(tcpSocket && (!tcpSocket->isValid() || tcpSocket->state() != QTcpSocket::ConnectedState)) {
            tcpSocket->abort();
            deleteSocket();
        }
        if(tcpSocket==nullptr) {
            tcpSocket = pendingConnection;
            status.conHost = tcpSocket->peerAddress();
            status.conPort = tcpSocket->peerPort();
            status.isFree = isFree();
            if(requestFinish)
                connect(tcpSocket, &QTcpSocket::bytesWritten,
                        this, &TcpServer::checkPendingData);
            connect(tcpSocket, &QTcpSocket::disconnected,
                    this, &TcpServer::handleDisconnect);
            m_connectionName = connectionName();
            qInfo().noquote() << logPrefix + " " + QString("Accepted connection from %1").arg(m_connectionName);
            sendTimer->start(0);
            updateStatus();
        } else {
            qInfo().noquote() << logPrefix + QString("Refused connection from %1").arg(connectionName(*pendingConnection));
            pendingConnection->abort();
            delete pendingConnection;
        }
    }
//    if(tcpSocket==nullptr){
//        qDebug()<<"No connections";
//    } else {
//        qDebug() << QString("Current connection: %1").arg(connectionName());
//    }
}


bool TcpServer::addEvent(const char *event, int len)
{
//    qDebug() << QTime::currentTime().toString("hh:mm:ss.zzz") << QString("addEvent: %1 bytes").arg(len);
    if(dropOnFree && tcpSocket==nullptr){
        numberBytesDropped += len;
        return true;
    }

    int writtenSize=0;
    Q_ASSERT(len < TCP_SERVER_BUFFER_SIZE); //todo complex partial writing
    int freeWriteSize = getFreeSize();

    while(freeWriteSize<len){
        //wait client read
        qWarning().noquote() << logPrefix + "Buffer is full, waiting client";
        writerMutex.lock();
        writerWC.wait(&writerMutex);
        freeWriteSize = getFreeSize();
        writerMutex.unlock();
    }
    if(requestFinish){
        requestFinish = false;
        disconnect(tcpSocket, &QTcpSocket::bytesWritten,
                   this, &TcpServer::checkPendingData);
    }
//    if(freeSpace.tryAcquire(len)){
//        qWarning()<<"Buffer is full, waiting client";
//        freeSpace.acquire(len);
//    }

    if(writePos+len > TCP_SERVER_BUFFER_SIZE){
        int tailSize = TCP_SERVER_BUFFER_SIZE-writePos;
        memcpy(data.data()+writePos, event, tailSize);
        writtenSize+=tailSize;
        writePos+=tailSize;
        if(writePos==TCP_SERVER_BUFFER_SIZE)
            writePos=0;
    }
    memcpy(data.data()+writePos, event+writtenSize, len-writtenSize);
    writePos += len-writtenSize;
    writtenSize+=len-writtenSize;
    if(writePos==TCP_SERVER_BUFFER_SIZE)
        writePos=0;
    return true;
}

void TcpServer::requestWorkCompleted()
{
    requestFinish = true;
    if(tcpSocket)
        connect(tcpSocket, &QTcpSocket::bytesWritten,
                this, &TcpServer::checkPendingData);
    checkPendingData();
}


int TcpServer::getFreeSize() const
{
//    return freeSpace.available();
    //If buffer fully written we use all-1 bytes and positions are equal
    //If buffer empty positions differs on 1
    if(readPos == writePos)
        return 0;

    int size=0;
    if(readPos < writePos)
        size = TCP_SERVER_BUFFER_SIZE-writePos+readPos;
    else
        size = readPos-writePos;

    return size;
}


int TcpServer::getDataSize() const
{
//    return TCP_SERVER_BUFFER_SIZE-getFreeSize();
    //If buffer fully written we use all-1 bytes and positions are equal
    if(readPos==writePos)
        return TCP_SERVER_BUFFER_SIZE;

    int size=0;
    if(readPos<writePos)
        size = writePos-readPos-1;
    else
        size = (TCP_SERVER_BUFFER_SIZE-readPos-1)+writePos;

    return size;
}
