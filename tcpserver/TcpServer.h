//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TCPSERVER_H
#define TCPSERVER_H

// Workaround for gcc < 4.8
#if __cplusplus < 201103L
#include <cstdatomic>
#else
#include <atomic>
#endif

#include <QObject>
#include <QByteArray>
#include <QHostAddress>
#include <QVector>
#include <QMutex>

class QTcpServer;
class QTcpSocket;
class QTimer;
class QElapsedTimer;
#include <QWaitCondition>
#include <QSemaphore>

struct TcpServerStatus
{
    TcpServerStatus():
        tcpPort(0),
        isFree(true),
        conPort(0),
        slow_connection_duration(0)
    {}
    quint16 tcpPort;
    bool isFree;
    QHostAddress conHost;
    quint16 conPort;
    qint64 slow_connection_duration;
};
Q_DECLARE_METATYPE(TcpServerStatus)

class TcpServer : public QObject
{
    Q_OBJECT

public:
    TcpServer(int port = defaultPort(), QObject *parent=nullptr, QString _logPrefix = QString());
    virtual ~TcpServer();
    bool isEnoughSpace(int size) const; // threadsafe
    int getServerPort() const { return tcpPort; } // deprecated
    bool isFree() const { return tcpSocket==nullptr; } // deprecated
    void setDropOnFree(bool v) { dropOnFree = v; }
    double getOccupancy() const;

signals:
    void bytesSent(quint64 value);
    void statusUpdated(const TcpServerStatus &);
    void workCompleted();

public slots:
    bool addEvent(const char *event, int len);

private slots:
    void requestWorkCompleted();
    void sendData();
    void updateConnections();
    void on_sendTimer_timeout();
    void deleteSocket();
    void handleDisconnect();
    void checkPendingData();
    void updateStatus();
public slots:
    void reset();

private:
    QString connectionName() const;
    QString connectionName(const QTcpSocket &socket) const;
    static int defaultPort() { return 0; } // 33400
    int getFreeSize() const;  // threadsafe
    int getDataSize() const;  // threadsafe

private:
    QString logPrefix;
    std::atomic<std::uint16_t> tcpPort;
    std::atomic_int writePos; // data position, where we start writing
    std::atomic_int readPos; // last transfered data position
    QTcpServer * const qTcpServer;
    QByteArray data;
    QTcpSocket *tcpSocket;
    QWaitCondition writerWC;
    QSemaphore freeSpace;
    QMutex writerMutex;
//    QMutex readerMutex;
    QTimer * const sendTimer;
    QTimer * const updateTimer;
    QElapsedTimer * const slow_connection_timer;
    qint64 slow_connection_duration;
    bool slowConnection;
    quint64 numberBytesSent;
    quint64 numberBytesDropped;
    QString m_connectionName;
    bool dropOnFree;
    bool requestFinish;
    TcpServerStatus status;
    bool test = true;
};

#endif // TCPSERVER_H
