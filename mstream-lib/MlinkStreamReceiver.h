//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef MLINKSTREAMRECEIVER_H
#define MLINKSTREAMRECEIVER_H

#include "socket_compat.h"

#include <QObject>
#include <QElapsedTimer>
#include <QVector>
#include <QMetaType>
#include <QSemaphore>
#include <QString>

#include "types.h"
#include "base/DeviceIndex.h"

typedef QPair<int, int> LoopInterval;
class QTimer;
class QTime;
class RawCheck;
class QString;

struct ReceiverStat {
    ReceiverStat (DeviceIndex _index=DeviceIndex(), int n = 0):
        index(_index)
    {
        clear(n);
    }
    void clear(int n) {
        noFree=0;
        didntHelp=0;
        free=0;
        checkFail=0;
        noData=0;
        pckSizeCnt.clear();
        pckSizeCnt.resize(n+1);
    }

    DeviceIndex index;
    quint32 noFree; // loop started with no free tasks
    quint32 didntHelp; // processEvents didn't help to free tasks
    quint32 free; // loops with free tasks
    quint32 checkFail; // check_header failed
    quint32 noData; // loops no data
    QVector<quint32> pckSizeCnt;
};
Q_DECLARE_METATYPE(ReceiverStat)

struct Task {
    Task() {
        buf = nullptr;
//        memset(buf, 0, BUFLEN);
        clear();
    }
    void clear() {
//        memset(buf, 0, BUFLEN);
//        fi = MLinkFrameInfo();
        len = 0;
        dataValid = false;
    }

    char *buf;
    uint len;
    MLinkFrameInfo fi;
    bool dataValid;
};
Q_DECLARE_METATYPE(Task)

class MlinkStreamReceiver : public QObject
{
    Q_OBJECT
public:
    explicit MlinkStreamReceiver(DeviceIndex index, const char *ipaddr, bool _bigFragId=false,
                                 quint16 hwBufSize=8, bool _doPrintStat = true);
    virtual ~MlinkStreamReceiver();
    void setAckSizeLimit(quint16 limit) { ackSizeLimit = limit; }
    uint16_t getLocalPort() const { return ntohs(si_me.sin_port); }
    QSemaphore *getDataSizeSem() { return &dataSize; }
    const Task &getPendingTask(uint shift) {
        return tasks[static_cast<int>((taskIndexSend+shift)%taskNum)];
    }
    void releaseDataSem(uint n) {
        shiftIndex(taskIndexSend, n);
        shiftIndex(taskIndexErase, n);
        freeSize.release(static_cast<int>(n));
    }
    QSemaphore testSem;

signals:
    void gotData(QVector<Task> tasks);
    void stat_from_receiver_updated(ReceiverStat stat);

private slots:
    void runLoop();
#if (! SEM_INTERACT)
    void sendWork();
#endif
public slots:
#if (! SEM_INTERACT)
    void releaseData(int shift);
#endif
    void connectToHardware();
    void deleteReceiver();

protected:
    void send_ack(const MLinkFrameInfo &fi);
    void send_ack(const std::vector<MLinkFrameInfo> &fiVec);
    ssize_t recvfrom(); // move it
    void bind_mstream();
    void init_si_other();
    inline bool hasFreeTask() { return (taskIndexWrite+1)%taskNum!=taskIndexErase; }
    inline bool hasFreeTask(int n) { return getFreeTask() >= n; }
    inline int getFreeTask() {
        return static_cast<int>((taskNum+taskIndexErase-taskIndexWrite-1)%taskNum);
    }
    inline int getTodoTask() {
        return static_cast<int>((taskIndexWrite+taskNum-taskIndexSend)%taskNum);
    }
    inline int getTaskToErase() {
        return static_cast<int>((taskIndexSend+taskNum-taskIndexErase)%taskNum);
    }
    void shiftIndex(uint &i,uint size=1) { i+=size; i%=taskNum; }
    void printLoopStat(int loopCount, int loopTime);

    int s_mstream;
    struct sockaddr_in si_other;
    struct sockaddr_in si_me;
    in_addr_t addr_other;
    const bool bigFragId; // fragmentId is 16-bit length
    QTime * const runLoopTime;
    QTimer * const runLoopTimer;
    RawCheck *const rc;
    QTimer *sendWorkTimer;

    QVector<Task> tasks;
    QVector<Task> sendtasks;
    QByteArray buf;
    const uint taskNum;
    uint taskIndexWrite;
    uint taskIndexSend;
    uint taskIndexErase;
//    int dataSeq;
    const int RECV_LIMIT;
    std::vector<mmsghdr> msgsTt;
    std::vector<iovec> iovecsTt;
    std::vector<MLinkFrameInfo> ackStack;
    quint16 ackSizeLimit;

    ReceiverStat stat;

    int timeoutCnt;
    QSemaphore freeSize;
    QSemaphore dataSize;
    uint16_t myPort;
    bool toDel;
    int runTimeCheckPeriodCnt;
    bool doPrintStat;
};

#endif // MLINKSTREAMRECEIVER_H
