#ifndef MSTREAMDUMPPARAMETERS_H
#define MSTREAMDUMPPARAMETERS_H

#include <QtGlobal>
#include <QHostAddress>

#include "types.h"
#include "base/DeviceIndex.h"

class MStreamDumpParameters
{
public:
    MStreamDumpParameters();
    QString logPrefix() const;
    QString getSerialIdStr(bool shortFormat=false) const;
    inline int getAcceptedRange() const { return acceptedRange; }
    void parseCommandLine();

    DeviceIndex deviceIndex;
    QHostAddress deviceAddress;
    int tcpPort;
    QString fileName;
    bool bigFragId; // fragmentId is 16-bit length
    quint16 hwBufSize;
    quint16 ackSizeLimit;

    void preRunInit(bool doMetricSenderInit=false);
private:

    int acceptedRange;
};

#endif // MSTREAMDUMPPARAMETERS_H
