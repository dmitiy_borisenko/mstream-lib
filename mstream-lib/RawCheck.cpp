//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "RawCheck.h"
#include <iostream>

using std::cerr;
using std::endl;
using std::hex;

RawCheck::RawCheck()
{
    n_bytes = 0;
    n_pkt = 0;
    n_pkt_bad = 0;
    n_pkt_miss = 0;
    exp_seq = 0;
    fragIdShift = 24;
}

bool RawCheck::check_header(const char *buf, size_t count, MLinkFrameInfo *fi)
{
    n_bytes += count;
    n_pkt ++;

    const MLinkFrameHdr *hdr = reinterpret_cast<const MLinkFrameHdr *>(buf);
    //            const uint32_t *h0 = (uint32_t *)&buf[sizeof(frame_hdr_t)];
    const uint32_t *mStreamHeader = reinterpret_cast<const uint32_t *>(&buf[sizeof(MLinkFrameHdr)+4]); // 2nd work of M-Stream header
    //            const int flen = *h0 & 0xFFFF;
    const int fid = static_cast<int>(*mStreamHeader>>fragIdShift); // M-Stream Fragment ID
     const int foff = static_cast<int>(*mStreamHeader & ((1<<fragIdShift) - 1));
    //            std::cout << std::hex << hdr->sync << " " << hdr->type << " " << hdr->src << "->" << hdr->dst << ", len " << std::dec
    //                      << hdr->len << ", seq " << hdr->seq << std::endl;
    if (hdr->sync != ML_FRAME_SYNC) {
        cerr << "Bad frame sync: " << hex << hdr->sync << endl;
        n_pkt_bad++;
        return false;
    }
    if (hdr->type != FRAME_TYPE_MSTREAM) {
        cerr << "Ignored frame of type: " << hex << hdr->type << endl;
        n_pkt_bad++;
        return false;
    }
    if (hdr->len == count * sizeof(quint32)){
        cerr << "Incomplete frame length: got " << count << "B, expected " << hdr->len << " 32bit words." << endl;
        n_pkt_bad++;
        return false;
    }
    //            cerr << "Received Fragment ID " << hex << fid << ", size " << dec << flen << endl;
    //            if ((hdr->len * 4) != result) {
    //                cerr << "Bad length " << dec << hdr->len << ", should be " << result/4 << endl;
    //                n_pkt_bad++;
    //                continue;
    //            }
    if (hdr->seq != exp_seq) {
        uint16_t n_lost = hdr->seq - exp_seq;
        //                cerr << "Sequence error: " << dec << hdr->seq << ", expected " << exp_seq << ", lost " << n_lost << endl;
        n_pkt_miss += n_lost;
        //                n_pkt_bad++;
    }
    exp_seq = hdr->seq + 1;
    fi->hdr = *hdr;
    fi->fid = fid;
    fi->foff = foff;
    return true;
}

void RawCheck::print_and_clear(uint64_t time_elapsed)
{
    double loss = 100.0 * n_pkt_miss / (n_pkt_miss + n_pkt);
    cerr << time_elapsed << ": Received " << n_pkt << " / missed " << n_pkt_miss << " packets (" << loss << "% loss)" //<< endl;
    << n_bytes << " bytes" << endl;
    n_bytes = 0; n_pkt = 0; n_pkt_miss = 0;
}
