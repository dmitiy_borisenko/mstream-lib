//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef MS_LIB_TYPES_H
#define MS_LIB_TYPES_H

#include <stdint.h>

#include <QByteArray>
#include <QMap>

const int fragment_header_size = 8;
enum { DATA_TYPE_DATAFRAME = 0xBE, DATA_TYPE_DATAFRAME_2 = 0xBC, DATA_TYPE_TRIGINFO = 0xBD };
enum { MS_SUB_TYPE_TRIG = 0x0, MS_SUB_TYPE_DATA = 0x1 };
enum { ML_FRAME_SYNC = 0x2A50 };
enum { FRAME_TYPE_CTRL_REQ = 0x0101, FRAME_TYPE_MSTREAM = 0x5354 };

const int BUFLEN = 8192; // max packet length
//const int BUFLEN = 999;
const uint32_t MAJOR_HDR_DATA_TYPE = 1;
const uint32_t MINOR_HDR_SUB_TYPE = 1;

//const int HW_PACKAGE_BUF_SIZE = 8;
const int FRAGMENTS_IN_PACKAGE_2_0 = 16;
const int FRAGMENTS_IN_PACKAGE_2_2 = 65;
//const int ACCEPT_RANGE_2_0 = HW_PACKAGE_BUF_SIZE * FRAGMENTS_IN_PACKAGE_2_0; // was 2*HW_BUF_SIZE
//const int ACCEPT_RANGE_2_2 = 2*(HW_PACKAGE_BUF_SIZE-1) * FRAGMENTS_IN_PACKAGE_2_2; // was 2*HW_BUF_SIZE
const int FRAGMENTS_SIZE_8_BIT = 1<<8;
const int FRAGMENTS_SIZE_16_BIT = 1<<16;


// === Device-mstream formats ===
// M-Link frame header
struct MLinkFrameHdr{
    uint16_t type; // Frame type. Transferred second.
    uint16_t sync; // Frame sync. Must be set equal to ML_FRAME_SYNC. Transferred first on 16-bit bus.
    uint16_t seq; // Frame sequence number. Incremented on every next frame sent in one direction.
    uint16_t len; // Frame length in 32-bit words. Valid length is 4 – 1023 (data size 0-1019)
    uint16_t src; // Source address
    uint16_t dst; // Destination address
};


struct __attribute__ ((packed)) MStreamFragmentHeader{
    uint16_t fragmentLength;
    uint8_t subType:2;
    uint8_t flgs:6;
    uint8_t deviceId;
    int getFragId(bool ver2) const { return fragmentOffsetId>>(ver2 ? 16 : 24); }
    int getFragOffset(bool ver2) const { return fragmentOffsetId&(ver2 ? 0xFFFF : 0xFFFFFF); }

    uint32_t fragmentOffsetId;
};

struct __attribute__ ((packed)) MStreamHeader{
    MStreamFragmentHeader mStreamHeader;

    uint32_t deviceSerial;

    uint32_t evNum : 24;
    quint8 userDefBits; // Exact trigpos (HRB && Sub==0)/ Channal number (ADC && Sub==1)
};

struct __attribute__ ((packed)) AdcMStreamTrigPayload{
    uint32_t taiSec;
    uint32_t taiNSecFlag;
    uint32_t lowCh;
    uint32_t hiCh;
};

#include <QMetaType>
// === Internal formats ===
struct MLinkFrameInfo {
    MLinkFrameHdr hdr;
    int fid;
    int foff;
};
Q_DECLARE_METATYPE(MLinkFrameInfo)

//typedef QPair<quint32, quint8> DeviceKey; // serial + deviceId (subtype-less)

struct BaseFragment
{
    BaseFragment() { clear(); }
    quint16 fragmentId;
    bool free;
    bool completed;
    bool closed;

    quint8 deviceId;
    quint32 deviceSerial;
    quint8 subType;
    quint32 evNum;

    QByteArray data; // <offset(quint32:24), data>
    QMap<int, int> parts;
    uint32_t expectedLen; // shift+size of last fragment
    uint32_t curLen; // sum of continious parts start from 0

    void clear(){
        free=true;
        completed=false;
        closed=false;

        deviceId=0;
        deviceSerial=0;
        subType=0;
        evNum=0;

        data.resize(0);
        parts.clear();
        expectedLen=0;
        curLen=0;
    }
};

// Adc64 channal uniting
struct AdcEventTrigInfo {
    AdcEventTrigInfo() { clear(); }
    void clear() {
        workedChannels=0;
        currentChannels=0;
        fragments.clear();
        totalDataSize=0;
        gotInfo=false;
        trigFrag=NULL;
        free = true;
        evNum = 0;
    }
    quint64 workedChannels;
    quint64 currentChannels;
    QMap<quint8, BaseFragment *> fragments; // channal to fragmentId with this ch.
    quint32 totalDataSize; // Sum of pure RawData
    bool gotInfo;
    BaseFragment *trigFrag;
    bool free;
    quint32 evNum;
};

#endif // MS_LIB_TYPES_H
