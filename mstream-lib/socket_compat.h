#ifndef SOCKET_COMPAT_H
#define SOCKET_COMPAT_H

#ifdef WIN32
#include <winsock2.h>
typedef int socklen_t;
typedef unsigned long in_addr_t;
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/select.h>
#endif

int socket_init();

#endif /* SOCKET_COMPAT_H */
