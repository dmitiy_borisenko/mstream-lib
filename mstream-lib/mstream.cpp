//
//    Copyright 2011 Ilja Slepnev & Alexey Shutov
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <exception>
#include <iostream>
#include <signal.h>
#include <QCoreApplication>

#include "mstream-lib/MStreamDump.h"
#include "mstream-lib/MStreamDumpParameters.h"
#include "syslog-msg-sender/SyslogMessageSender.h"
#include "metric-sender/MetricSender.h"

static std::function<void()> callbackMStreamTerm;
void mstreamTerm(int signum) {
    qInfo() << "Interrupt signal (" << signum << ") received.\n";

    if(callbackMStreamTerm) {
        callbackMStreamTerm();
    }
    signal(SIGTERM, nullptr);
    raise(SIGTERM);
}

int main(int argc, char *argv[])
{
    int ret;

    QCoreApplication::setOrganizationName("AFI Electronics");
    QCoreApplication::setOrganizationDomain("local");
    QCoreApplication::setApplicationName("MStream");

#ifdef QT_DEBUG
    qInfo() << "Debug version";
#endif
    qInfo() << "Starting up";
    SyslogMessageSender::instance().install();
    MetricSender::instance().install();

    try {
        QCoreApplication app(argc, argv);
        MStreamDumpParameters msParams;
        msParams.parseCommandLine();

        MStreamDump *mStreamDumper = new MStreamDump(msParams);
        callbackMStreamTerm = std::bind(&MStreamDump::terminate, mStreamDumper);
        signal(SIGTERM, mstreamTerm);

        QObject::connect(mStreamDumper, SIGNAL(destroyed(QObject*)), &app, SLOT(quit()));

        ret = app.exec();
    }
    catch (std::exception &exc) {
        std::cerr << exc.what() << std::endl;
        qCritical() << exc.what();
        exit(1);
    }
    SyslogMessageSender::instance().remove();

    qInfo() << "Shutdown complete";
    return ret;
}
