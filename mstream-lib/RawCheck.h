//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef RAWCHECK_H
#define RAWCHECK_H

#include "mstream-lib/types.h"
#include "stddef.h"

class RawCheck {
public:
    RawCheck();
    bool check_header(const char *buf, size_t count, MLinkFrameInfo *fi);
    void print_and_clear(uint64_t time_elapsed);
    void setBigFragId(bool isBigFragId=true)
    { fragIdShift = isBigFragId ? 16 : 24; }
protected:
    uint64_t n_bytes, n_pkt, n_pkt_bad, n_pkt_miss;
    uint16_t exp_seq;
    int fragIdShift;
};

#endif // RAWCHECK_H
