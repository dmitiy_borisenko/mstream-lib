HEADERS += \
    $$PWD/MlinkStreamReceiver.h \
    $$PWD/MStreamDump.h \
    $$PWD/MStreamDumpParameters.h \
    $$PWD/MstreamPorts.h \
    $$PWD/RawCheck.h \
    $$PWD/socket_compat.h \
    $$PWD/types.h \
    $$PWD/MStreamFileWriter.h \

SOURCES +=  \
    $$PWD/MlinkStreamReceiver.cpp \
    $$PWD/MStreamDump.cpp \
    $$PWD/MStreamDumpParameters.cpp \
    $$PWD/RawCheck.cpp \
    $$PWD/socket_compat.cpp \
    $$PWD/MStreamFileWriter.cpp \

QT     += network

win32 {
    LIBS += -lws2_32
}
