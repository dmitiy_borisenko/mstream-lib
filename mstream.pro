include(mstream.pri)
include(stdconfig.pri)

TARGET = mstream
TEMPLATE = app

QT     -= gui
DEFINES += SEM_INTERACT=1
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

include($$PWD/../multicast_listener/multicast_listener.pri)
include($$PWD/../PNPServer/PNPServer.pri)
include($$PWD/../mldiscover/mldiscover_core.pri)
include($$PWD/../svn_rev.pri)
HEADERS += $$PWD/../mpd-common/DaqConfig.h \
           $$PWD/../mpd-common/PrintInfo.h \
           $$PWD/../mpd-common/SyslogMessageSender.h \
           $$PWD/../mpd-common/MetricSender.h \
           $$PWD/../mpd-common/MetricSenderHelper.h \
           $$PWD/../mpd-common/threadQuit.h

SOURCES += $$PWD/../mpd-common/DaqConfig.cpp \
           $$PWD/../mpd-common/PrintInfo.cpp \
           $$PWD/../mpd-common/SyslogMessageSender.cpp \
           $$PWD/../mpd-common/MetricSender.cpp \
           $$PWD/../mpd-common/MetricSenderHelper.cpp \
           $$PWD/../mpd-common/threadQuit.cpp \
           $$PWD/mstream.cpp

# set install path for binaries
target.path    = /bin
INSTALLS += target
